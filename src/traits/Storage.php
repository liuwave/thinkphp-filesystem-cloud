<?php

namespace liuwave\filesystem\traits;

trait Storage
{
    public function getUrl(string $path): string
    {
        if (str_starts_with($path, '/')) {
            return $path;
        }
        
        return isset($this->config[ 'url' ]) && $this->config[ 'url' ] ? $this->config[ 'url' ].DIRECTORY_SEPARATOR.$path : $path;
    }
}
