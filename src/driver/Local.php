<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace liuwave\filesystem\driver;

use liuwave\filesystem\traits\Storage;

/**
 * Class Local
 * @package liuwave\filesystem\driver
 */
class Local extends \think\filesystem\driver\Local
{
    use Storage;
}
