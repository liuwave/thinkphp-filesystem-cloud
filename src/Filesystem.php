<?php
declare(strict_types=1);

namespace liuwave\filesystem;

class Filesystem extends \think\Filesystem
{
    protected $namespace = '\\liuwave\\filesystem\\driver\\';
}
